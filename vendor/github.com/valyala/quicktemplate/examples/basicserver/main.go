// The following line is needed for generating go code from templates
// with `go generate`.
// See https://blog.golang.org/generate for more info.
// Quicktemplate compiler (qtc) must be installed before running
// `go generate`:
//
//     go get -u github.com/valyala/quicktemplate/qtc
//
//go:generate qtc -dir=templates
package main

import (
	"fmt"
	"log"
	"math/rand"

	"github.com/valyala/fasthttp"
	"github.com/valyala/quicktemplate/examples/basicserver/templates"
	"utils"
)

func main() {
	log.Printf("starting the server at http://localhost:8080 ...")
	err := fasthttp.ListenAndServe("localhost:8080", requestHandler)
	if err != nil {
		log.Fatalf("unexpected error in server: %s", err)
	}
}

func requestHandler(ctx *fasthttp.RequestCtx) {
	switch string(ctx.Path()) {
	case "/":
		mainPageHandler(ctx)
	case "/table":
		tablePageHandler(ctx)
	case "/base":
		basePageHandler(ctx)
	case "/jtpay":
		jtpayPageHandler(ctx)
	case "/jtpayreturn":
		jtpayreturnPageHandler(ctx)
	case "/download":
		downloadPageHandler(ctx)
	default:
		errorPageHandler(ctx)
	}
	ctx.SetContentType("text/html; charset=utf-8")
}

func downloadPageHandler(ctx *fasthttp.RequestCtx) {
	p := &templates.Download{
        Title: "https://bbbbvb.cn/happy/download",
	}
	templates.WriteDownloadTemplate(ctx, p)
}

func jtpayreturnPageHandler(ctx *fasthttp.RequestCtx) {
	p := &templates.JtpayReturn{
        P3_money: "11",
        P5_orderid: "1111",
        P6_productcode: "WX",
        LocalTime: utils.Time2Str(utils.LocalTime()),
	}
	templates.WriteJtpayReturnTemplate(ctx, p)
}

func jtpayPageHandler(ctx *fasthttp.RequestCtx) {
	p := &templates.JtpayOrder{
        P1_yingyongnum: "1111",
        P2_ordernumber: "1111",
        P3_money: "1111",
        P6_ordertime: "1111",
        P7_productcode: "1111",
        P8_sign: "1111",
        P9_signtype: "1111",
        P25_terminal: "1111",
	}
	templates.WriteJtpayOrderTemplate(ctx, p)
}

func basePageHandler(ctx *fasthttp.RequestCtx) {
	p := &templates.BasePage{
		//CTX: ctx,
	}
	templates.WritePageTemplate(ctx, p)
}

func mainPageHandler(ctx *fasthttp.RequestCtx) {
	p := &templates.MainPage{
		CTX: ctx,
	}
	templates.WritePageTemplate(ctx, p)
}

func tablePageHandler(ctx *fasthttp.RequestCtx) {
	rowsCount := ctx.QueryArgs().GetUintOrZero("rowsCount")
	if rowsCount == 0 {
		rowsCount = 10
	}
	p := &templates.TablePage{
		Rows: generateRows(rowsCount),
	}
	templates.WritePageTemplate(ctx, p)
}

func errorPageHandler(ctx *fasthttp.RequestCtx) {
	p := &templates.ErrorPage{
		Path: ctx.Path(),
	}
	templates.WritePageTemplate(ctx, p)
	ctx.SetStatusCode(fasthttp.StatusBadRequest)
}

func generateRows(rowsCount int) []string {
	var rows []string
	for i := 0; i < rowsCount; i++ {
		r := fmt.Sprintf("row %d", i)
		if rand.Intn(20) == 0 {
			r = "bingo"
		}
		rows = append(rows, r)
	}
	return rows
}
