/**********************************************************
 * Author        : piaohua
 * Email         : 814004090@qq.com
 * Last modified : 2018-01-13 10:56:33
 * Filename      : desk_logic.go
 * Description   : 内部方法
 * *******************************************************/
package main

import (
	"gohappy/data"
	"gohappy/glog"
	"gohappy/pb"

	"github.com/AsynkronIT/protoactor-go/actor"
)

// internal function

//NewDesk 新建一张牌桌
func NewDesk(deskData *data.DeskData) *Desk {
	desk := &Desk{
		roles:  make(map[string]*data.DeskRole),
		seats:  make(map[uint32]*data.DeskSeat),
		router: make(map[string]string),
		stopCh: make(chan struct{}),
	}
	desk.DeskData = deskData
	return desk
}

//InitDesk 初始化
func (t *Desk) InitDesk() {
	t.DeskGame = new(data.DeskGame)
	switch t.DeskData.Rtype {
	case int32(pb.ROOM_TYPE0): //自由
	case int32(pb.ROOM_TYPE1): //私人
		t.DeskPriv = new(data.DeskPriv)
		t.DeskPriv.PrivScore = make(map[string]int64)
		t.DeskPriv.Joins = make(map[string]uint32)
	case int32(pb.ROOM_TYPE2): //百人
		t.DeskFree = new(data.DeskFree)
		t.DeskFree.Dealers = make(map[string]int64) //上庄列表,userid: carry
		t.DeskFree.Carry = SysCarry
		t.freeInit()
	}
}

//房间消息广播
func (t *Desk) broadcast(msg interface{}) {
	for _, v := range t.roles {
		if v == nil {
			continue
		}
		if v.Pid == nil {
			continue
		}
		if v.Offline {
			continue
		}
		v.Pid.Tell(msg)
	}
}

//房间消息广播(除userid外)
func (t *Desk) broadcast2(userid string, msg interface{}) {
	for k, v := range t.roles {
		if v == nil {
			continue
		}
		if v.Pid == nil {
			continue
		}
		if v.Offline {
			continue
		}
		if k != userid {
			v.Pid.Tell(msg)
		}
	}
}

//房间消息广播(除seat外)
func (t *Desk) broadcast3(seat uint32, msg interface{}) {
	if v, ok := t.seats[seat]; ok && v != nil {
		t.broadcast2(v.Userid, msg)
	}
}

//给玩家发送消息
func (t *Desk) send2userid(userid string, msg interface{}) {
	if v, ok := t.roles[userid]; ok && v != nil {
		if v.Offline {
			return
		}
		v.Pid.Tell(msg)
	}
}

//给位置发送消息
func (t *Desk) send2seat(seat uint32, msg interface{}) {
	if v, ok := t.seats[seat]; ok && v != nil {
		t.send2userid(v.Userid, msg)
	}
}

//给玩家发送消息,离线也发送
func (t *Desk) send3userid(userid string, msg interface{}) {
	if v, ok := t.roles[userid]; ok && v != nil {
		v.Pid.Tell(msg)
	}
}

//给位置发送消息,离线也发送
func (t *Desk) send3seat(seat uint32, msg interface{}) {
	if v, ok := t.seats[seat]; ok && v != nil {
		t.send3userid(v.Userid, msg)
	}
}

//获取路由
func (t *Desk) getRouter(ctx actor.Context) string {
	glog.Debugf("getRouter %s", ctx.Sender().String())
	return t.router[ctx.Sender().String()]
}

//获取进程pid,离线也可发送
func (t *Desk) getPid(userid string) *actor.PID {
	if v, ok := t.roles[userid]; ok && v != nil {
		return v.Pid
	}
	return nil
}

//获取玩家数据
func (t *Desk) getPlayer(userid string) *data.User {
	if v, ok := t.roles[userid]; ok && v != nil {
		return v.User
	}
	return nil
}

//获取玩家数据
func (t *Desk) getUserBySeat(seat uint32) *data.User {
	if v, ok := t.seats[seat]; ok && v != nil {
		return t.getPlayer(v.Userid)
	}
	return nil
}

//获取位置
func (t *Desk) getSeat(userid string) uint32 {
	if v, ok := t.roles[userid]; ok && v != nil {
		return v.Seat
	}
	return 0
}

//获取位置
func (t *Desk) getUserid(seat uint32) string {
	if v, ok := t.seats[seat]; ok && v != nil {
		return v.Userid
	}
	return ""
}

//玩家是否在线
func (t *Desk) isOnline(userid string) bool {
	if v, ok := t.roles[userid]; ok && v != nil {
		return !v.Offline
	}
	return false
}

//设置玩家是否离线
func (t *Desk) setOffline(userid string, offline bool) {
	if v, ok := t.roles[userid]; ok && v != nil {
		v.Offline = offline
	}
}

//获取手牌
func (t *Desk) getHandCards(seat uint32) []uint32 {
	//房间类型 百人场
	if t.DeskData.Rtype == int32(pb.ROOM_TYPE2) &&
		t.DeskFree != nil {
		return t.DeskFree.Cards[seat]
	}
	//房间类型 非百人场
	if v, ok := t.seats[seat]; ok && v != nil {
		return v.Cards
	}
	glog.Errorf("getHandCards %d", seat)
	t.printOver()
	return []uint32{}
	//panic(fmt.Sprintf("getHandCards error:%d", seat))
}

//玩家牌力
func (t *Desk) getPower(seat uint32) uint32 {
	if v, ok := t.seats[seat]; ok && v != nil {
		return v.Power
	}
	return 0
}

//位置下注
func (t *Desk) getBets(seat uint32) int64 {
	if v, ok := t.seats[seat]; ok && v != nil {
		return v.Bet
	}
	return 0
}

// vim: set foldmethod=marker foldmarker=//',//.:

//获取游戏参与者数量（准备的人才是参与者）
func (t *Desk) getReadyNum() int64 {
	num := int64(0)
	for _, v := range t.seats {
		if v.Ready {
			num++
		}
	}
	return num
}

//获取玩家可抢庄倍数
func (t *Desk) getDealerPermission(userid string, seat uint32) int64 {

	permission := int64(0)
	num := int64(10) //普通5倍，疯狂10倍
	if t.DeskData.Mode == 0 {
		num = 5
	}

	if v, ok := t.roles[userid]; ok && v != nil {
		//规定：抢庄倍数=携带金币/(底注×闲家人数×10倍x玩家最大加倍数)
		permission = v.User.Coin / (int64(t.DeskData.Ante) * (t.getReadyNum() - int64(1)) * num * int64(3))
		t.seats[seat].DealerPermission = permission
	}

	return permission
}

//获取玩家可下注倍数
func (t *Desk) getBetPermission(userid string, seat uint32) int64 {
	permission := int64(0)
	num := int64(15)
	if t.DeskData.Mode == 0 { //牌型倍数:普通5倍，疯狂10倍
		num = 5
	}
	if v, ok := t.roles[userid]; ok && v != nil {
		//规定：玩家加倍数=玩携带金币/(底注×闲家人数×10倍×抢庄倍数)
		dealer := t.seats[t.DeskGame.DealerSeat]
		if dealer == nil {
			return permission
		}
		if dealer.BetPermission < 0 { //庄家加倍数
			if dealer.DealerN > 0 {
				permission = t.roles[dealer.Userid].User.Coin / (int64(t.DeskData.Ante) * (t.getReadyNum() - int64(1)) * int64(dealer.DealerN) * num)
			} else {
				permission = t.roles[dealer.Userid].User.Coin / (int64(t.DeskData.Ante) * (t.getReadyNum() - int64(1)) * num)
			}

			t.seats[t.DeskGame.DealerSeat].BetPermission = permission

			dealer = t.seats[t.DeskGame.DealerSeat] //数据更新，重新赋值
		}
		if t.DeskGame.DealerSeat != seat { //玩家加倍数
			if dealer.DealerN > 0 {
				permission = v.User.Coin / (int64(t.DeskData.Ante) * int64(dealer.DealerN) * num)
			} else {
				permission = v.User.Coin / (int64(t.DeskData.Ante) * num)
			}
			if permission > dealer.BetPermission { //庄家金币不足，玩家是不能加倍的，玩家自己金币不足也是不能加倍的
				permission = dealer.BetPermission
			}
			t.seats[seat].BetPermission = permission
		}
	}

	return permission
}

//是否有玩家有抢庄权限
func (t *Desk) isDealerPermission() bool {
	for _, v := range t.seats {
		if v.Ready && v.DealerPermission > 0 {
			return true
		}
	}
	return false
}

//是否有玩家有⏬权限
func (t *Desk) isBetPermission() bool {
	for _, v := range t.seats {
		if v.Ready && v.BetPermission > 0 && t.DeskGame.DealerSeat != t.roles[v.Userid].Seat {
			return true
		}
	}
	return false
}
